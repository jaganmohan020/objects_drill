function invert(obj) {

    let inverted = {};
    
    Object.entries(obj).forEach(entry => {
      let key = entry[0];
      let value = entry[1];
      inverted[value] = key
    })

    return inverted;
  }

  module.exports = invert; 
