cb = x => x + 5;
function mapObject(obj, cb) {
  const mapped = {}
  for ( let prop in obj ){
    mapped[prop] = cb(obj[prop]);
  }
  return mapped;
}


module.exports = mapObject;